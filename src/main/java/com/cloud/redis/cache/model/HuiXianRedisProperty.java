package com.cloud.redis.cache.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description :   .
 *
 * @author : LuShunNeng
 * @date : Created in 2020-02-22 19:29
 */
@Data
@ConfigurationProperties(prefix = "com.huixian.redis")
public class HuiXianRedisProperty {


    /**
     * 是否开启缓存功能
     */
    private Boolean enableCache = true;
}
