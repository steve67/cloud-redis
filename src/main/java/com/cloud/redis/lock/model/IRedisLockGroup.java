package com.cloud.redis.lock.model;

import java.util.concurrent.TimeUnit;

/**
 * create at  2018/08/23 18:13
 *
 * @author yanggang
 */
public interface IRedisLockGroup {

    String getGroup();

    /**
     * 锁时间,默认60秒
     */
    @Deprecated
    default Integer getLockSecond() {
        return 60;
    }


    /**
     * 锁时间,默认60秒
     */
    @Deprecated
    default Integer getWaitLockSecond() {
        return 0;
    }

    /**
     * 锁时间,默认60秒,
     * -1 代表不设置过期时间，会使用看门狗自检查模式 默认 每10秒一轮，每轮过期时间延长为当前时间的30秒
     */
    default long getLockTime() {
        return 60000;
    }


    /**
     * 锁等待时间,默认0秒 不等待
     */
    default long getWaitLockTime() {
        return 0;
    }


    /**
     * 锁时间单位
     */
    default TimeUnit getTimeUnit() {
        return TimeUnit.MILLISECONDS;
    }
}
