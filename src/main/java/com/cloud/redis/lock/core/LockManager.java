package com.cloud.redis.lock.core;


import com.cloud.redis.lock.model.IRedisLockGroup;

/**
 * Description :   .
 *
 * @author : andr
 * @date : Created in 2020/9/8 下午3:41
 */
public interface LockManager {

    /**
     * 加锁
     * @param group  组名
     * @param key  key
     * @return
     */
    boolean lock(IRedisLockGroup group, String key);

    /**
     * 是否被锁
     * @param group
     * @param key
     * @return
     */
    boolean isLocked(IRedisLockGroup group, String key);

    /**
     * 解锁
     * @param group 组名
     * @param key key
     */
    void unLock(IRedisLockGroup group, String key);
}
