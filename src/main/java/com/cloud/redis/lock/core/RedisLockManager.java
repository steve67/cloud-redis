package com.cloud.redis.lock.core;

import com.cloud.redis.core.KeyGenerator;
import com.cloud.redis.lock.model.IRedisLockGroup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * Description :   .
 *
 * @author : andr
 * @date : Created in 2020/9/8 下午3:44
 */
@Slf4j
@Service
public class RedisLockManager implements LockManager {

    @Autowired
    private KeyGenerator keyGenerator;

    @Autowired
    private RedissonClient redissonClient;


    @Override
    public boolean lock(IRedisLockGroup group, String key) {
        if (group == null || StringUtils.isBlank(group.getGroup()) || StringUtils.isBlank(key)) {
            log.error("参数错误，redisson加锁失败,group:{},key:{}", group, key);
            return false;
        }
        String generateLockKey = keyGenerator.generateLockKey(group.getGroup(), key);
        return coreLock(generateLockKey,group.getWaitLockTime(),group.getLockTime(),group.getTimeUnit());
    }


    @Override
    public boolean isLocked(IRedisLockGroup group, String key) {
        if (group == null || StringUtils.isBlank(group.getGroup()) || StringUtils.isBlank(key)) {
            log.error("参数错误，redisson查询锁失败,group:{},key:{}", group, key);
            return false;
        }
        String generateLockKey = keyGenerator.generateLockKey(group.getGroup(), key);
        RLock lock = redissonClient.getLock(generateLockKey);
        return lock.isLocked();
    }

    /**
     * 加锁的核心方法
     * @param key
     * @param waitTime
     * @param lockTime
     * @param timeUnit
     * @return
     */
    private boolean coreLock(String key,long waitTime,long lockTime,TimeUnit timeUnit) {
        RLock lock = redissonClient.getLock(key);
        boolean flag = false;
        try {
            flag = lock.tryLock(waitTime,lockTime, timeUnit);
        }catch (Exception e){
            log.error("分布式锁try lock失败，generateLockKey{}",key);
            log.error("分布式锁try lock失败，异常为",e);
        }
        return flag;
    }

    @Override
    public void unLock(IRedisLockGroup group, String key) {
        if (group == null || StringUtils.isBlank(group.getGroup()) || StringUtils.isBlank(key)) {
            log.error("参数错误，redisson解锁失败,group:{},key:{}", group, key);
            return;
        }
        String generateLockKey = keyGenerator.generateLockKey(group.getGroup(), key);
        RLock lock = redissonClient.getLock(generateLockKey);
        lock.forceUnlock();
    }
}
