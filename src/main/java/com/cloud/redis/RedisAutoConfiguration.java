package com.cloud.redis;

import com.cloud.redis.cache.core.CacheAnnotationInterceptor;
import com.cloud.redis.cache.core.RedisCacheManager;
import com.cloud.redis.cache.model.HuiXianRedisProperty;
import com.cloud.redis.core.ExpressionParserConfig;
import com.cloud.redis.core.KeyGenerator;
import com.cloud.redis.core.RedisConfig;
import com.cloud.redis.core.RedisService;
import com.cloud.redis.core.RedissonConfig;
import com.cloud.redis.lock.core.RedisLockManager;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

/**
 * create at  2018/08/22 10:53
 *
 * @author yanggang
 */
@Configuration
@Import(value = {
        CacheAnnotationInterceptor.class,
        KeyGenerator.class
        , RedisConfig.class
        , RedissonConfig.class
        , RedisService.class
        , RedisCacheManager.class
        , RedisLockManager.class
        , ExpressionParserConfig.class})
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableConfigurationProperties(HuiXianRedisProperty.class)
public class RedisAutoConfiguration {

}
