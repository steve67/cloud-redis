package com.cloud.redis.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * Description :   引入EL表达式
 *
 * @author : yangcong
 * @motto : good good work，day day up
 * @date : Created in 2023/3/12 1:58 下午
 */
@Configuration
public class ExpressionParserConfig {
    @Bean
    public ExpressionParser expressionParser() {
        return new SpelExpressionParser();
    }

    @Bean
    public LocalVariableTableParameterNameDiscoverer localVariableTableParameterNameDiscoverer() {
        return new LocalVariableTableParameterNameDiscoverer();
    }
}