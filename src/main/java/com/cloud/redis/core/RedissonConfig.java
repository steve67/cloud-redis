package com.cloud.redis.core;

import io.netty.channel.nio.NioEventLoopGroup;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description :   .
 *
 * @author : andr
 * @date : Created in 2020/9/8 下午2:56
 */
@Configuration
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private String port;
    @Value("${spring.redis.password}")
    private String password;
    @Value("${spring.redis.database:0}")
    private int database;
    @Value("${spring.redisson.timeout:10000}")
    private int timeout;

    @Value("${spring.redisson.idle-connection-timeout:10000}")
    private int idleConnectionTimeout;
    @Value("${spring.redisson.connect-timeout:10000}")
    private int connectTimeout;
    @Value("${spring.redisson.retry-attempts:3}")
    private int retryAttempts;
    @Value("${spring.redisson.retry-interval:1500}")
    private int retryInterval;
    @Value("${spring.redisson.connection-minimum-idle-size:5}")
    private int connectionMinimumIdleSize;
    @Value("${spring.redisson.connection-pool-size:64}")
    private int connectionPoolSize;
    @Value("${spring.redisson.lock-watchdog-timeout:30000}")
    private Long lockWatchdogTimeout;
    @Value("${spring.redisson.subscription-connection-minimum-idle-size:1}")
    private int subscriptionConnectionMinimumIdleSize;
    @Value("${spring.redisson.subscription-connection-pool-size:50}")
    private int subscriptionConnectionPoolSize;



    @Bean(destroyMethod = "shutdown")
    RedissonClient redissonClient(){
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://" + host + ":" + port)
                .setConnectionMinimumIdleSize(connectionMinimumIdleSize)
                .setConnectionPoolSize(connectionPoolSize)
                .setDatabase(database)
                .setRetryAttempts(retryAttempts)
                .setRetryInterval(retryInterval)
                .setTimeout(timeout)
                .setConnectTimeout(connectTimeout)
                .setIdleConnectionTimeout(idleConnectionTimeout)
                .setPassword(password)
                .setSubscriptionConnectionMinimumIdleSize(subscriptionConnectionMinimumIdleSize)
                .setSubscriptionConnectionPoolSize(subscriptionConnectionPoolSize);
        config.setEventLoopGroup(new NioEventLoopGroup());
        config.setLockWatchdogTimeout(lockWatchdogTimeout);
        return Redisson.create(config);
    }
}
